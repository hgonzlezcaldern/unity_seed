﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Componente que permite cargar el siguiente nivel desde una pantalla de carga dinámica de forma asíncrona. Se presupone que el nivel que ha puesto la pantalla de carga debe
/// haber establecido previamente la variable SCENE_SECTION/NEXT_SCENE con el nombre de la escena a cargar.
/// </summary>
public class AsyncLoader : AComponent
{
    private bool loadingInit = false;

    protected override void Update () {

        if (!loadingInit)
        {
            loadingInit = true;
            
            Load();
        }
    }
    
    protected void Load()
    {
        // TODO 1: hacer carga asincrona ChangeAsyncScene de la escena guardada en SceneMgr.SCENE_SECTION y SceneMgr.NEXT_SCENE
    }

}
