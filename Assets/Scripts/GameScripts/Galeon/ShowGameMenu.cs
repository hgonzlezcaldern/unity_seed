﻿using UnityEngine;
using System.Collections;

public class ShowGameMenu : AComponent {
	
	public string m_menuSceneName;
	public enum TPushSceneType { PUSH_SCENE, POP_SCENE};
	public TPushSceneType m_type = TPushSceneType.PUSH_SCENE;
	public bool m_clearReturnScene = false;
	
	protected override void Awake()
	{
		base.Awake();
        //TODO 1: nos registras en el input al boton return con OnReturnPressed.
	}
	
	protected void OnReturnPressed()
	{
		//muestro el menu..
        //TODO 2 if type == TPushSceneType.PUSH_SCENE => PushScene else ReturnScene
	}
	
	protected override void OnDestroy() 
	{
		base.OnDestroy();
		//TODO 3 desregistrar el return.
	}
	
	/*protected virtual void Tick(float deltaTime){}
	protected virtual void Init(){}
	
	protected virtual void End() {}*/
}
